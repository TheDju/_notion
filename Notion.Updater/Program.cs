﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Notion.Updater
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            const string serverAddress = "https://www.notion.so";
            const string outputDirectory = "wwwroot";
            Regex resourceUrlRegex = new Regex(@"\/[a-z\-\._=\/0-9]+\.(?:png|ico|css|js|jpg|woff|gif|svg)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            HttpClient httpClient = new HttpClient();

            // 1. Download the main HTML file
            Console.WriteLine("Downloading Notion HTML ...");
            string htmlContent = await httpClient.GetStringAsync(serverAddress);

            // 2. List all existing resources
            Console.WriteLine("Checking existing resources ...");
            string[] existingResources = Directory.GetFiles(outputDirectory, "*.*", SearchOption.AllDirectories)
                .Select(f => f.Substring(outputDirectory.Length + 1).Replace("\\", "/"))
                .OrderBy(f => f)
                .ToArray();

            // 3. Download every referenced resource
            Console.WriteLine("Downloading new resources ...");
            MatchCollection resourceMatches = resourceUrlRegex.Matches(htmlContent);
            List<string> resources = resourceMatches
                .Select(m => m.Value.Replace("www.notion.so", "").TrimStart('/'))
                .ToList();

            resources.Add("favicon.ico");
            resources.Add("config.json");

            int resourcesCount = 0;
            while (resources.Count != resourcesCount)
            {
                string[] newResources = resources.Skip(resourcesCount).ToArray();
                resourcesCount = resources.Count;

                foreach (string resource in newResources)
                {
                    string resourcePath = Path.Combine(outputDirectory, resource);

                    if (!File.Exists(resourcePath))
                    {
                        string resourceDirectory = Path.GetDirectoryName(resourcePath);
                        if (!Directory.Exists(resourceDirectory))
                            Directory.CreateDirectory(resourceDirectory);

                        string resourceUrl = serverAddress + "/" + resource;

                        Console.WriteLine($"- Downloading {resource}");

                        using (Stream resourceStream = await httpClient.GetStreamAsync(resourceUrl))
                        using (FileStream fileStream = new FileStream(resourcePath, FileMode.Create))
                            resourceStream.CopyTo(fileStream);
                    }

                    if (resource.EndsWith(".png") || resource.EndsWith(".woff") || resource.EndsWith(".gif") || resource.EndsWith(".ico") || resource.EndsWith(".jpg"))
                        continue;

                    string resourceContent = File.ReadAllText(resourcePath);
                    resourceMatches = resourceUrlRegex.Matches(resourceContent);

                    string[] referencedResources = resourceMatches
                        .Select(m => m.Value.Replace("www.notion.so", "").TrimStart('/'))
                        .Where(r => !r.EndsWith(".js") && !r.EndsWith(".ts") && !r.EndsWith(".tsx") && !r.EndsWith(".css"))
                        .Where(r => (!r.Contains("react") || r.EndsWith(".png")) && !r.Contains("node"))
                        .ToArray();

                    resources.AddRange(referencedResources);
                }
            }

            // 4. Clean old resources
            Console.WriteLine("Cleaning old resources ...");
            string[] oldResources = existingResources
                .Where(r => !resources.Contains(r))
                .ToArray();

            foreach (string resource in oldResources)
            {
                string resourcePath = Path.Combine(outputDirectory, resource);
                File.Delete(resourcePath);

                string resourceDirectory = Path.GetDirectoryName(resourcePath);
                while (!string.IsNullOrWhiteSpace(resourceDirectory) && Directory.GetFileSystemEntries(resourceDirectory).Length == 0)
                {
                    Directory.Delete(resourceDirectory);
                    resourceDirectory = Path.GetDirectoryName(resourceDirectory);
                }
            }

            // 5. Patch HTML and configuration
            Console.WriteLine("Patching files ...");

            htmlContent = htmlContent
                .Replace("@", "@@")
                .Replace("\tgtag(", "\t//gtag(");

            htmlContent = @"@{
    string baseUrl = UrlHelper.GetBaseUrl(Context.Request);
    string authClientId = Environment.GetEnvironmentVariable(""NOTION_AUTH_CLIENTID"");
    string embedlyKey = Environment.GetEnvironmentVariable(""NOTION_EMBEDLY_KEY"");
}" + htmlContent;

            string configPath = Path.Combine(outputDirectory, "config.json");
            string configContent = File.ReadAllText(configPath);

            htmlContent = htmlContent.Replace("<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-59922529-2\"></script>", "");

            int scriptsPosition = htmlContent.IndexOf("<script type=\"text/javascript\" src=\"/");
            htmlContent = htmlContent.Remove(scriptsPosition) + "<script type=\"text/javascript\">window.CONFIG = " + configContent + ";</script>" + htmlContent.Substring(scriptsPosition);

            string htmlPath = @"Views\Home\Index.cshtml";
            File.WriteAllText(htmlPath, htmlContent);

            Regex configRegex = new Regex(@",CONFIG:{.+?(?=}}}\))}}");

            string vendorsResource = resources.First(r => r.StartsWith("vendors-"));
            string vendorsResourcePath = Path.Combine(outputDirectory, vendorsResource);
            string vendorsContent = File.ReadAllText(vendorsResourcePath);

            vendorsContent = configRegex.Replace(vendorsContent, ",CONFIG:window.CONFIG");

            File.WriteAllText(vendorsResourcePath, vendorsContent);

            string appResource = resources.First(r => r.StartsWith("app-"));
            string appResourcePath = Path.Combine(outputDirectory, appResource);
            string appContent = File.ReadAllText(appResourcePath);

            appContent = appContent.Replace("logs-01.loggly.com", "127.0.0.1");

            File.WriteAllText(appResourcePath, appContent);
        }
    }
}
