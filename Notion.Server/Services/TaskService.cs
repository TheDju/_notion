﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using Notion.Server.Helpers;

namespace Notion.Server.Services
{
    public class TaskToken
    {
        public string Id { get; set; }
        public Task<dynamic> Task { get; set; }
    }

    public class TaskService
    {
        private readonly DatabaseService database;

        private ConcurrentDictionary<string, TaskToken> pendingTasks = new ConcurrentDictionary<string, TaskToken>();

        public TaskService(DatabaseService database)
        {
            this.database = database;
        }

        private TaskToken EnqueueTask(string id, Func<dynamic> task)
        {
            TaskToken token = new TaskToken()
            {
                Id = id,
                Task = Task.Run(task)
            };

            pendingTasks.TryAdd(id, token);

            return token;
        }
        public TaskToken GetTaskResult(string id)
        {
            if (!pendingTasks.TryGetValue(id, out TaskToken token))
                return null;

            return token;
        }

        public TaskToken CopyBlock(dynamic task)
        {
            string id = task.id;
            dynamic data = task.data;

            string sourceId = data.sourceId ?? data.sourceBlockId;
            string targetId = data.targetId ?? data.targetBlockId;

            return EnqueueTask(id, () =>
            {
                dynamic source = database.Blocks.Find(sourceId);
                dynamic target = database.Blocks.Find(targetId);

                // Copy block information
                CopyBlockProperties(source, target);

                // Duplicate all children
                string[] content = (source.content as JArray).Values<string>().ToArray();
                foreach (string block in content)
                    CopyBlockInternal(block, target);

                // Save the block
                database.Blocks.Update(target);

                return new JObject()
                {
                    { "recordMap", new JObject() {
                        { "block", new JObject(NotionHelper.WrapObject(database, target, "editor")) }
                    } }
                };
            });
        }
        public TaskToken DeleteSpace(dynamic task)
        {
            string id = task.id;
            dynamic data = task.data;

            return EnqueueTask(id, () =>
            {
                string spaceId = (string)data.spaceId;
                dynamic space = database.Spaces.Find(spaceId);

                // Delete all blocks
                string[] pageIds = (space.pages as JArray).Values<string>().ToArray();
                dynamic[] pages = database.Blocks.FindAll(pageIds);

                foreach (dynamic page in pages)
                    DeleteBlock(page);

                // Remove space views
                dynamic[] spaceViews = database.SpaceViews
                    .Where(s => s.space_id == spaceId)
                    .ToArray();

                foreach (dynamic spaceView in spaceViews)
                {
                    string userRootId = spaceView.parent_id;
                    dynamic userRoot = database.UserRoots.Find(userRootId);

                    JArray userRootSpaceViews = userRoot.space_views as JArray;
                    int index = userRootSpaceViews.Values<string>().ToList().IndexOf((string)spaceView.id);
                    userRootSpaceViews.RemoveAt(index);
                    database.UserRoots.Update(userRoot);

                    database.SpaceViews.Remove(spaceView);
                }

                // Remove space
                database.Spaces.Remove(space);

                return new JObject();
            });
        }

        private void CopyBlockInternal(string sourceId, dynamic targetParent, bool preserveTargetPermissions = true)
        {
            dynamic source = database.Blocks.Find(sourceId);

            dynamic target = new JObject()
            {
                { "id", Guid.NewGuid().ToString("D") },
                { "version", 1 },
                { "alive", true },
                { "parent_id", targetParent.id },
                { "parent_table", "block" }
            };

            CopyBlockProperties(source, target, preserveTargetPermissions);

            if (targetParent.content == null)
                targetParent.content = new JArray();

            (targetParent.content as JArray).Add(target.id);

            if (source.content != null)
            {
                string[] content = (source.content as JArray).Values<string>().ToArray();
                foreach (string block in content)
                    CopyBlockInternal(block, target, preserveTargetPermissions);
            }

            database.Blocks.Add(target);
        }
        private void CopyBlockProperties(dynamic source, dynamic target, bool preserveTargetPermissions = true)
        {
            JObject targetObject = target;

            foreach (JProperty property in source.Properties())
            {
                if (property.Name == "id")
                    continue;
                if (property.Name == "version")
                    continue;
                if (property.Name == "alive")
                    continue;
                if (property.Name == "content")
                    continue;
                if (property.Name == "parent_id" || property.Name == "parent_table")
                    continue;
                if (property.Name == "created_by" || property.Name == "created_time" || property.Name == "last_edited_by" || property.Name == "last_edited_time")
                    continue;
                if (preserveTargetPermissions && property.Name == "permissions")
                    continue;

                target[property.Name] = property.Value;
            }
        }
        private void DeleteBlock(dynamic block)
        {
            block.alive = false;
            database.Blocks.Update(block);

            string[] content = (block.content as JArray)?.Values<string>().ToArray();
            if (content == null)
                return;

            dynamic[] subBlocks = database.Blocks.FindAll(content);
            foreach (dynamic subBlock in subBlocks)
                DeleteBlock(subBlock);
        }
    }
}