﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Notion.Server.Services
{
    public class AuthenticatedUser
    {
        public string Token { get; set; }
        public string UserId { get; set; }
    }

    public class AuthenticationService
    {
        public bool Enabled { get; }

        private readonly DatabaseService databaseService;
        private readonly string directory = "Authentication";
        private readonly string defaultUserId = "206765ad-a061-44ff-9519-1ca8a53dc1d5";

        private readonly bool enableAutomaticRegistration;
        private readonly string[] allowedRegistrations;

        private readonly string oAuthClientId;
        private readonly string oAuthClientSecret;

        private Dictionary<string, string> pendingTokens = new Dictionary<string, string>();
        private Dictionary<string, AuthenticatedUser> authenticatedUsers = new Dictionary<string, AuthenticatedUser>();

        public AuthenticationService(DatabaseService database, bool resetUsers = false)
        {
            this.databaseService = database;

            string enabledText = Environment.GetEnvironmentVariable("NOTION_AUTH_ENABLED");
            Enabled = enabledText?.ToLower() != "false";

            if (Enabled)
            {
                string enableAutomaticRegistrationText = Environment.GetEnvironmentVariable("NOTION_AUTH_AUTOREGISTRATION");
                enableAutomaticRegistration = enableAutomaticRegistrationText?.ToLower() == "true";

                string allowedRegistrationsText = Environment.GetEnvironmentVariable("NOTION_AUTH_ALLOWEDREGISTRATIONS");
                if (!string.IsNullOrEmpty(allowedRegistrationsText))
                {
                    allowedRegistrations = allowedRegistrationsText
                        .ToLower()
                        .Split(new[] { ',', ';', ' ', '|' }, StringSplitOptions.RemoveEmptyEntries);
                }

                oAuthClientId = Environment.GetEnvironmentVariable("NOTION_AUTH_CLIENTID");
                oAuthClientSecret = Environment.GetEnvironmentVariable("NOTION_AUTH_CLIENTSECRET");

                if (string.IsNullOrEmpty(oAuthClientId) || string.IsNullOrEmpty(oAuthClientSecret))
                    throw new Exception("You must provide OAuth client ID and secret");

                if (resetUsers)
                {
                    if (Directory.Exists(directory))
                        Directory.Delete(directory, true);
                }

                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                Refresh();
            }
            else
            {
                if (database.Users.Find(defaultUserId) == null)
                {
                    dynamic user = new JObject()
                    {
                        { "id", defaultUserId },
                        { "version", 1 },
                        { "given_name", "John" },
                        { "family_name", "Doe" },
                        { "type", "personally" },
                        { "persona", "programmer" },
                        { "onboarding_completed", true },
                        { "mobile_onboarding_completed", true },
                    };
                    database.Users.Add(user);

                    dynamic space = new JObject()
                    {
                        { "id", Guid.NewGuid().ToString("D") },
                        { "version", 1 },
                        { "name", "My space" },
                        { "domain", "space-" + user.GetHashCode() },
                        { "permissions", new JArray() { new JObject {
                            { "role", "editor" },
                            { "type", "user_permission" },
                            { "user_id", user.id },
                        } } }
                    };
                    database.Spaces.Add(space);

                    dynamic spaceView = new JObject()
                    {
                        { "id", Guid.NewGuid().ToString("D") },
                        { "version", 1 },
                        { "space_id", space.id },
                        { "parent_id", user.id },
                        { "parent_table", "user_root" },
                        { "alive", true },
                        { "notify_mobile", false },
                        { "notify_desktop", false },
                        { "notify_email", false },
                    };
                    database.SpaceViews.Add(spaceView);

                    dynamic userRoot = new JObject()
                    {
                        { "id", user.id },
                        { "version", 1 },
                        { "space_views", new JArray(new string[] { spaceView.id }) }
                    };
                    database.UserRoots.Add(userRoot);
                }
            }
        }

        private void Refresh()
        {
            string[] jsonFiles = Directory.GetFiles(directory, "*.json");
            foreach (string jsonFile in jsonFiles)
            {
                string clientId = Path.GetFileNameWithoutExtension(jsonFile);
                string json;

                using (StreamReader reader = new StreamReader(jsonFile))
                    json = reader.ReadToEnd();

                AuthenticatedUser authenticatedUser = JsonConvert.DeserializeObject<AuthenticatedUser>(json);
                authenticatedUsers[clientId] = authenticatedUser;
            }
        }

        public string GetAuthenticationURL(string clientId, string baseUrl, string callbackType = "popup")
        {
            string token = Guid.NewGuid().ToString("D");

            JObject stateObject = new JObject()
            {
                { "callbackType", callbackType },
                { "encryptedToken", token }
            };

            string stateJson = stateObject.ToString();
            string state = Convert.ToBase64String(Encoding.UTF8.GetBytes(stateJson));

            pendingTokens[clientId] = token;

            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                ["access_type"] = "offline",
                ["scope"] = "profile email https://www.googleapis.com/auth/userinfo.profile",
                ["state"] = state,
                ["prompt"] = "select_account",
                ["response_type"] = "code",
                ["client_id"] = oAuthClientId,
                ["redirect_uri"] = $"{baseUrl.TrimEnd('/')}/googlepopupcallback"
            };

            string url = "https://accounts.google.com/o/oauth2/auth?" + string.Join("&", parameters.Select(p => $"{p.Key}={WebUtility.UrlEncode(p.Value)}"));
            return url;
        }
        public bool ValidateAuthenticationCallback(string clientId, string state)
        {
            if (!pendingTokens.TryGetValue(clientId, out string pendingState))
                return false;

            pendingTokens.Remove(clientId);

            if (pendingState != state)
                return false;

            return true;
        }

        public AuthenticatedUser AuthenticateDefaultUser(string clientId)
        {
            AuthenticatedUser authenticatedUser = new AuthenticatedUser()
            {
                Token = GenerateAuthenticationToken(),
                UserId = defaultUserId
            };

            authenticatedUsers[clientId] = authenticatedUser;

            return authenticatedUser;
        }
        public AuthenticatedUser AuthenticateUser(string clientId, dynamic user)
        {
            AuthenticatedUser authenticatedUser = new AuthenticatedUser()
            {
                Token = GenerateAuthenticationToken(),
                UserId = (string)user.id
            };

            authenticatedUsers[clientId] = authenticatedUser;

            string path = GetPath(clientId);
            EnsurePathExist(path);

            using (StreamWriter writer = new StreamWriter(path))
            {
                string json = JsonConvert.SerializeObject(authenticatedUser, new JsonSerializerSettings() { Formatting = Formatting.Indented });
                writer.WriteLine(json);
            }

            return authenticatedUser;
        }
        public dynamic CheckAndGetUser(string clientId, string token, string userId)
        {
            if (!Enabled)
                return databaseService.Users.Find(defaultUserId);

            if (!authenticatedUsers.TryGetValue(clientId, out AuthenticatedUser authenticatedUser))
                return null;

            if (authenticatedUser.Token != token)
                return null;
            if (authenticatedUser.UserId != userId)
                return null;

            return databaseService.Users.FirstOrDefault(u => u.id == userId);
        }
        public void LogoutUser(string clientId, string token, string userId)
        {
            dynamic user = CheckAndGetUser(clientId, token, userId);
            if (user == null)
                return;

            if (!authenticatedUsers.TryGetValue(clientId, out AuthenticatedUser authenticatedUser))
                return;

            string path = GetPath(clientId);
            EnsurePathExist(path);

            File.Delete(path);

            authenticatedUsers.Remove(clientId);
        }

        public dynamic GetTokenFromCode(string baseUrl, string code)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                Task<HttpResponseMessage> response = httpClient.PostAsync("https://www.googleapis.com/oauth2/v4/token", new FormUrlEncodedContent(new Dictionary<string, string>()
                {
                    ["code"] = code,
                    ["client_id"] = oAuthClientId,
                    ["client_secret"] = oAuthClientSecret,
                    ["redirect_uri"] = $"{baseUrl.TrimEnd('/')}/googlepopupcallback",
                    ["grant_type"] = "authorization_code"
                }));

                HttpResponseMessage result = response.Result;
                string content = result.Content.ReadAsStringAsync().Result;

                dynamic token = JObject.Parse(content);
                return token;
            }
        }
        public dynamic GetUserProfile(string tokenType, string accessToken)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(tokenType, accessToken);

                Task<HttpResponseMessage> response = httpClient.GetAsync("https://www.googleapis.com/plus/v1/people/me/openIdConnect");
                HttpResponseMessage result = response.Result;
                string content = result.Content.ReadAsStringAsync().Result;

                dynamic userProfile = JObject.Parse(content);
                return userProfile;
            }
        }

        public bool IsRegistrationAllowed(string email)
        {
            bool allowedRegistration = enableAutomaticRegistration || (allowedRegistrations != null && allowedRegistrations.Contains(email.ToLower()));
            return allowedRegistration;
        }
        public dynamic RegisterUser(dynamic userProfile)
        {
            // FIXME: Implement correct onboarding

            dynamic user = new JObject()
            {
                { "id", Guid.NewGuid().ToString("D") },
                { "version", 1 },
                { "given_name", userProfile.given_name },
                { "family_name", userProfile.family_name },
                { "profile_photo", userProfile.picture },
                { "email", userProfile.email },
                { "type", "personally" },
                { "persona", "programmer" },
                { "onboarding_completed", false },
                { "mobile_onboarding_completed", false }
            };

            databaseService.Users.Add(user);

            /*dynamic space = new JObject()
            {
                { "id", Guid.NewGuid().ToString("D") },
                { "version", 1 },
                { "name", "My space" },
                { "domain", "space-" + user.GetHashCode() },
                { "permissions", new JArray() { new JObject {
                    { "role", "editor" },
                    { "type", "user_permission" },
                    { "user_id", user.id },
                } } }
            };

            database.Spaces.Add(space);

            dynamic spaceView = new JObject()
            {
                { "id", Guid.NewGuid().ToString("D") },
                { "version", 1 },
                { "space_id", space.id },
                { "parent_id", user.id },
                { "parent_table", "user_root" },
                { "alive", true },
                { "notify_mobile", false },
                { "notify_desktop", false },
                { "notify_email", false },
            };

            database.SpaceViews.Add(spaceView);*/

            dynamic userRoot = new JObject()
            {
                { "id", user.id },
                { "version", 1 },
                { "space_views", new JArray(new string[] { /*spaceView.id*/ }) }
            };

            databaseService.UserRoots.Add(userRoot);

            return user;
        }

        private static string GenerateAuthenticationToken()
        {
            char[] characters = "123456789abcdef".ToArray();

            Random random = new Random();
            string token = new string(Enumerable.Range(0, 156).Select(i => characters[random.Next(characters.Length)]).ToArray());

            return token;
        }
        private string GetPath(string clientId)
        {
            return Path.Combine(directory, clientId + ".json");
        }
        private void EnsurePathExist(string path)
        {
            string directory = Path.GetDirectoryName(path);

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }
    }
}