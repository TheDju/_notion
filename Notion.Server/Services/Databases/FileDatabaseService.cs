﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Notion.Server.Services
{
    public class FileDatabaseService : DatabaseService
	{
        public override Collection this[string name] => objects.GetOrAdd(name, new Collection(this, name));

        private ConcurrentDictionary<string, Collection> objects = new ConcurrentDictionary<string, Collection>();
        private string directory;

        public FileDatabaseService(string databaseDirectory, bool createNew = false)
        {
            directory = databaseDirectory;

            if (createNew)
                Clear();

            Refresh();
            LoadTemplates();
        }

        public override void Clear()
        {
            if (Directory.Exists(directory))
            {
                foreach (string file in Directory.GetFiles(directory))
                    File.Delete(file);

                foreach (string dir in Directory.GetDirectories(directory))
                {
                    if (dir.Contains(".git"))
                        continue;

                    Directory.Delete(dir, true);
                }
            }

            objects.Clear();
        }
        public override void Refresh()
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            objects.Clear();

            string[] jsonFiles = Directory.GetFiles(directory, "*.json", SearchOption.AllDirectories);
            Parallel.ForEach(jsonFiles, jsonFile =>
            {
                if (jsonFile.Contains(".git"))
                    return;

                string type = Path.GetFileName(Path.GetDirectoryName(jsonFile));
                string json;

                using (StreamReader reader = new StreamReader(jsonFile))
                    json = reader.ReadToEnd();

                try
                {
                    dynamic obj = JObject.Parse(json);
                    this[type].AddInternal(obj);
                }
                catch (Exception exception)
                {
                    Console.WriteLine($"Error while reading {jsonFile}. {exception.Message}");
                }
            });
        }

        protected override void OnAdd(string type, dynamic obj)
        {
            string path = GetPath(type, (string)obj.id);
            EnsurePathExist(path);

            using (StreamWriter writer = new StreamWriter(path))
            {
                string json = JsonConvert.SerializeObject(obj, new JsonSerializerSettings() { Formatting = Formatting.Indented });
                writer.WriteLine(json);
            }
        }
        protected override void OnUpdate(string type, dynamic obj)
        {
            string path = GetPath(type, (string)obj.id);
            EnsurePathExist(path);

            using (StreamWriter writer = new StreamWriter(path))
            {
                string json = JsonConvert.SerializeObject(obj, new JsonSerializerSettings() { Formatting = Formatting.Indented });
                writer.WriteLine(json);
            }
        }
        protected override void OnRemove(string type, dynamic obj)
        {
            string path = GetPath(type, (string)obj.id);
            EnsurePathExist(path);

            File.Delete(path);
        }

        private string GetPath(string type, string id)
        {
            return Path.Combine(directory, type, id + ".json");
        }
        private void EnsurePathExist(string path)
        {
            string directory = Path.GetDirectoryName(path);

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }
    }
}