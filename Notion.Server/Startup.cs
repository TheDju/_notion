﻿using System;
using System.Linq;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json;

using Notion.Server.Services;

namespace Notion
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCompression()
                    .AddResponseCaching()
                    .AddMvc()
                    .AddJsonOptions(options => options.SerializerSettings.Formatting = Formatting.Indented);

            string createNewDatabaseText = Environment.GetEnvironmentVariable("NOTION_DATABASE_CREATENEW");
            bool createNewDatabase = createNewDatabaseText?.ToLower() == "true";

            DatabaseService database = new FileDatabaseService("Database", createNewDatabase);
            if (!database.Blocks.Any())
                database.LoadTemplates();

            services.AddSingleton(typeof(DatabaseService), database);
            services.AddSingleton(typeof(AuthenticationService));
            services.AddSingleton(typeof(FileService));
            services.AddSingleton(typeof(TaskService));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();
            app.UseResponseCompression();
            app.UseResponseCaching();

            app.UseMvc(routes =>
            {
                routes.MapSpaFallbackRoute("spa", new { controller = "Home", action = "Index" });
            });
        }
    }
}
