﻿using System;

using Microsoft.AspNetCore.Mvc;

using Notion.Server.Services;

namespace Notion.Server.Controllers
{
    public class HomeController : BaseController
    {
        private readonly DatabaseService database;
        private readonly AuthenticationService authentication;

        public HomeController(DatabaseService database, AuthenticationService authentication) : base(database)
        {
            this.database = database;
            this.authentication = authentication;
        }

        [ResponseCache(Duration = 604800)] // 1 week
        public IActionResult Index()
        {
            string clientId = ClientId;

            dynamic user = GetCurrentUser(authentication);
            if (user == null)
            {
                if (!authentication.Enabled)
                {
                    var token = authentication.AuthenticateDefaultUser(clientId);

                    Token = token.Token;
                    UserId = token.UserId;
                }
                else
                {
                    bool redirect = Token != null || UserId != null;

                    Token = null;
                    UserId = null;

                    if (redirect)
                    {
                        // TODO: Redirect to home
                    }
                }
            }
            
            return View(nameof(Index));
        }

        [HttpGet("/logincallback")]
        public IActionResult LoginCallback()
        {
            return Index();
        }

        [HttpGet("/logoutcallback")]
        public IActionResult LogoutCallback()
        {
            return Index();
        }

        [HttpGet("/refresh")]
        public IActionResult RefreshDatabase()
        {
            database.Refresh();
            return new ObjectResult($"Database refreshed on {DateTime.Now}");
        }
    }
}
