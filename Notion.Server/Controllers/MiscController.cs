﻿using Microsoft.AspNetCore.Mvc;

using Notion.Server.Services;

namespace Notion.Server.Controllers
{
    public class MiscController : BaseController
    {
        public MiscController(DatabaseService databaseService) : base(databaseService)
        {
        }

        [HttpPost("inputs/{key}/tag/{type}")]
        public object LogglyInputs(string key, string type)
        {
            return Ok();
        }

        [HttpGet("primus/")]
        public object PrimusGet(string sessionId, string _primuscb = null, string EIO = null, string transport = null, string t = null, string b64 = null)
        {
            return Content(@"96:0{""sid"":""pkC_i3WhYVPLcUX6A1Ls"", ""upgrades"":[""websocket""],""pingInterval"":25000,""pingTimeout"":5000}");
        }

        [HttpPost("primus/")]
        public object PrimusPost(string sessionId, string _primuscb = null, string EIO = null, string transport = null, string t = null, string b64 = null)
        {
            return Content("ok");
        }
    }
}
