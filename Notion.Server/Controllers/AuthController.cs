﻿using System;
using System.Net;
using System.Text;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;

using Newtonsoft.Json.Linq;

using Notion.Server.Services;

namespace Notion.Server.Controllers
{
    public class AuthController : BaseController
    {
        private readonly DatabaseService databaseService;
        private readonly AuthenticationService authenticationService;

        public AuthController(DatabaseService databaseService, AuthenticationService authentication) : base(databaseService)
        {
            this.databaseService = databaseService;
            this.authenticationService = authentication;
        }

        [HttpGet("/googlepopupredirect")]
        public IActionResult GooglePopupRedirect(string callbackType, bool redirectToAuth)
        {
            string authenticationUrl = authenticationService.GetAuthenticationURL(ClientId, BaseUrl, callbackType);
            return Redirect(authenticationUrl);
        }

        [HttpGet("/googlepopupcallback")]
        public IActionResult GooglePopupCallback()
        {
            string queryString = Request.QueryString.Value;
            var queryDictionary = QueryHelpers.ParseQuery(queryString);

            if (!queryDictionary.TryGetValue("state", out var stateValues))
                return BadRequest();

            string state = WebUtility.UrlDecode(stateValues[0]);
            state = Encoding.UTF8.GetString(Convert.FromBase64String(state));
            JObject stateObject = JObject.Parse(state);

            string callbackType = stateObject["callbackType"].Value<string>();

            if (callbackType == "redirect")
                return Redirect("/oauth2callback" + queryString);
            else
                return View("PopupCallback");
        }

        [HttpGet("/oauth2callback")]
        public IActionResult OAuth2Callback()
        {
            return View("../Home/Index");
        }

        [HttpGet("/server/logout")]
        public IActionResult Logout()
        {
            authenticationService.LogoutUser(ClientId, Token, UserId);

            Token = null;
            UserId = null;

            return Redirect("/logoutcallback");
        }
    }
}
