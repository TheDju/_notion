﻿using System;
using System.IO;
using System.Linq;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Notion.Server.Helpers;
using Notion.Server.Services;

namespace Notion.Server.Controllers
{
    public abstract class BaseController : Controller
    {
        public string BaseUrl => UrlHelper.GetBaseUrl(Request);

        public string ClientId
        {
            get
            {
                string clientId = Request.Cookies["__cfduid"];

                if (clientId == null)
                {
                    clientId = GenerateClientId();
                    Response.Cookies.Append("__cfduid", clientId, new CookieOptions() { HttpOnly = true, SameSite = SameSiteMode.None, Expires = DateTime.Now.AddDays(30) });
                }

                return clientId;
            }
        }
        public string Token
        {
            get => Request.Cookies["token_v2"];
            set
            {
                if (value == null)
                    Response.Cookies.Delete("token_v2");
                else
                {
                    if (UrlHelper.UseHttps)
                        Response.Cookies.Append("token_v2", value, new CookieOptions() { HttpOnly = true, Secure = true, SameSite = SameSiteMode.None, Expires = DateTime.Now.AddDays(30) });
                    else
                        Response.Cookies.Append("token_v2", value, new CookieOptions() { HttpOnly = true, SameSite = SameSiteMode.None, Expires = DateTime.Now.AddDays(30) });
                }
            }
        }
        public string UserId
        {
            get => Request.Cookies["userId"];
            set
            {
                if (value == null)
                    Response.Cookies.Delete("userId");
                else
                {
                    if (UrlHelper.UseHttps)
                        Response.Cookies.Append("userId", value, new CookieOptions() { Secure = true, SameSite = SameSiteMode.None, Expires = DateTime.Now.AddDays(30) });
                    else
                        Response.Cookies.Append("userId", value, new CookieOptions() { Expires = DateTime.Now.AddDays(30) });
                }
            }
        }

        private readonly DatabaseService database;

        public BaseController(DatabaseService database)
        {
            this.database = database;
        }

        public dynamic GetParameters()
        {
            using (StreamReader bodyReader = new StreamReader(Request.Body))
            using (JsonTextReader jsonReader = new JsonTextReader(bodyReader))
            {
                return JObject.Load(jsonReader);
            }
        }
        public dynamic GetCurrentUser(AuthenticationService authentication)
        {
            string clientId = ClientId;
            string token = Token;
            string userId = UserId;

            if (clientId == null || token == null || userId == null)
                return null;

            return authentication.CheckAndGetUser(clientId, token, userId);
        }

        public int GetRoleLevel(string role) => NotionHelper.GetRoleLevel(role);
        public dynamic GetPermission(dynamic obj, dynamic user) => NotionHelper.GetPermission(database, obj, user);
        public dynamic StripPermissions(dynamic obj) => NotionHelper.StripPermissions(obj);

        public JProperty WrapObject(dynamic obj, dynamic element) => NotionHelper.WrapObject(database, obj, element);

        private static string GenerateClientId()
        {
            char[] characters = "123456789abcdef".ToArray();

            Random random = new Random();
            string token = new string(Enumerable.Range(0, 43).Select(i => characters[random.Next(characters.Length)]).ToArray());

            return token;
        }
    }
}
