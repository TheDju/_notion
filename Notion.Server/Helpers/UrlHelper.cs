﻿using System;

using Microsoft.AspNetCore.Http;

namespace Notion.Server.Helpers
{
    public static class UrlHelper
    {
        public static bool UseHttps { get; }

        static UrlHelper()
        {
            string useHttpsText = Environment.GetEnvironmentVariable("NOTION_SERVER_HTTPS");
            UseHttps = useHttpsText?.ToLower() == "true";
        }

        public static string GetBaseUrl(HttpRequest httpRequest)
        {
            string baseUrl = $"{(UseHttps ? Uri.UriSchemeHttps : Uri.UriSchemeHttp)}://{httpRequest.Host}";
            return baseUrl;
        }
    }
}
