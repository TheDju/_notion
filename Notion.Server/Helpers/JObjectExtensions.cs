﻿namespace Newtonsoft.Json.Linq
{
    public static class JObjectExtensions
    {
        public static JProperty FindProperty(this JObject obj, params string[] path)
        {
            JProperty property = new JProperty("temp", obj);

            foreach (string step in path)
            {
                if (property.Value == null || property.Value.Type == JTokenType.Null)
                    property.Value = new JObject(new JProperty(step, null));
                else if (property.Value is JObject value && value.Property(step) == null)
                    value[step] = null;

                property = (property.Value as JObject).Property(step);
            }

            return property;
        }
    }
}
