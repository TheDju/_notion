﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Notion.Unpacker
{
    class Program
    {
        static void Main(string[] args)
        {
            string workingDirectory = @"..\..\..\Notion.Server";
            Environment.CurrentDirectory = workingDirectory;

            string inputFile = Directory.GetFiles("wwwroot", "app-*.js").First();
            string appDirectory = @"..\Notion.Unpacker\app";
            string outputFile = appDirectory + @"\app.js";

            // Unpack the main file
            if (false)
            using (StreamReader reader = new StreamReader(inputFile))
            {
                StreamWriter writer = new StreamWriter(outputFile);

                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();

                    if (line.StartsWith("/*!******"))
                        continue;

                    if (line.StartsWith("  !*** ./"))
                    {
                        outputFile = Path.Combine(appDirectory, line.Trim(new[] { ' ', '!', '*' }));

                        string outputDirectory = Path.GetDirectoryName(outputFile);
                        if (!Directory.Exists(outputDirectory))
                            Directory.CreateDirectory(outputDirectory);

                        writer.Dispose();
                        writer = new StreamWriter(outputFile);

                        Console.WriteLine(outputFile.Replace("\\", "/"));

                        reader.ReadLine();
                        continue;
                    }

                    if (line.StartsWith("/*"))
                    {
                        int separator = line.IndexOf("*/");

                        string comment = line.Substring(0, separator + 2);
                        writer.WriteLine(comment);

                        line = line.Substring(separator + 2);
                        if (line.Trim() == "")
                            continue;
                    }

                    if (line.StartsWith("function(e,t,r){\"use strict"))
                    {
                        line = line
                            .Substring(16, line.Length - 18)
                            .Replace("!==", " !## ")
                            .Replace("===", "~~~")
                            .Replace("==", "~~")
                            .Replace("=", " = ")
                            .Replace("&&", " && ")
                            .Replace("||", " || ")
                            .Replace("return\"", "return \"")
                            .Replace(":", ": ")
                            .Replace(",", ", ")
                            .Replace("~~", " == ")
                            .Replace("~~~", " === ")
                            .Replace("!##", " !== ")
                            .Replace("+", " + ")
                            .Replace("!1", "false")
                            .Replace("!0", "true")
                            .Replace("if(", "if (")
                            ;

                        int index = 0;
                        int indentLevel = 0;

                        while (true)
                        {
                            int separator = line.IndexOfAny(new[] { ';', '{', '}' }, index);

                            if (separator == -1)
                            {
                                writer.WriteLine(line.Substring(index));
                                break;
                            }
                            
                            string indent = new string(' ', indentLevel * 4);
                            writer.Write(indent + line.Substring(index, separator - index));

                            if (line[separator] == '}')
                                indentLevel--;
                            if (indentLevel < 0)
                                indentLevel = 0;

                            indent = new string(' ', indentLevel * 4);

                            if (line[separator] == ';')
                                writer.WriteLine(";");
                            else if (line[separator] == '{')
                                writer.WriteLine(" {");
                            else
                            {
                                writer.WriteLine();
                                writer.WriteLine(indent + line[separator]);
                            }

                            if (line[separator] == '{')
                                indentLevel++;

                            index = separator + 1;
                        }
                    }
                    else
                        writer.WriteLine(line);
                }

                writer.Dispose();
            }

            // Deobfsucate files
            string[] sourceFiles = Directory.GetFiles(appDirectory + @"\src", "*.*", SearchOption.AllDirectories);
            Regex variableRequireRegex = new Regex(@"[,r]? (?<Variable>[a-z]+) = r\(\/\*! (?<Path>[^ ]+) \*\/[^\)]+\)", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex requireRegex = new Regex(@"r\(\/\*! (?<Path>[^ ]+) \*\/[^\)]+\)", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            int n = 0;
            foreach (string sourceFile in sourceFiles.Skip(1))
            {
                if (sourceFile.EndsWith(".ts.ts"))
                    continue;

                string content = File.ReadAllText(sourceFile);

                while (content.StartsWith("/*!"))
                    content = content.Substring(content.IndexOf(Environment.NewLine) + Environment.NewLine.Length); ;

                Dictionary<string, string> requires = new Dictionary<string, string>();
                content = variableRequireRegex.Replace(content, m =>
                {
                    requires[m.Groups["Variable"].Value] = m.Groups["Path"].Value;
                    return "";
                });

                content = content.Replace("va;", "");
                content = content.Replace("va,", "var");

                foreach (var require in requires)
                {
                    string variable = Path.GetFileName(require.Value)
                        .Replace("-", "")
                        ;

                    content = $"var {variable} = require(\"{require.Value}\");" + Environment.NewLine + content;
                    content = Regex.Replace(content, $@"([^\w\/]){require.Key}([\.\(])", $"$1{variable}$2");
                }

                content = requireRegex.Replace(content, "require(\"$1\")");

                File.WriteAllText(sourceFile + ".ts", content);

                if (n++ > 10)
                    break;
            }
        }
    }
}
